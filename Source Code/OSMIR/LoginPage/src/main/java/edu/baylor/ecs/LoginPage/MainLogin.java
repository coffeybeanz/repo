package edu.baylor.ecs.LoginPage;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Graphics;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class MainLogin {

	private JFrame frame;
	private JTextField textUserName;
	private JPasswordField textPassword;
	private static CreateNewLogin cnl;
	
	public class ImagePanel extends JPanel{
		private static final long serialVersionUID = 1L;
		private BufferedImage image;

	    public ImagePanel() {
	       try {                
	          image = ImageIO.read(new File("OSMIR.png"));
	       } catch (IOException ex) {
	            // handle exception...
	       }
	    }

	    @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        g.drawImage(image, 0, 0, this);          
	    }

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainLogin window = new MainLogin();
					cnl = new CreateNewLogin();
					window.frame.setVisible(true);
					cnl.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(143, 174, 74, 22);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(143, 202, 74, 22);
		frame.getContentPane().add(lblPassword);
		
		textUserName = new JTextField();
		textUserName.setBounds(229, 175, 86, 20);
		frame.getContentPane().add(textUserName);
		textUserName.setColumns(10);
		
		textPassword = new JPasswordField();
		textPassword.setBounds(229, 202, 86, 22);
		frame.getContentPane().add(textPassword);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textUserName.getText().equals("CoffeyBeanz") && textPassword.getText().equals("Baylor!")) {
					System.out.println("success");
				}
				else {
					System.out.println("fail");
				}
			}
		});
		btnSubmit.setBounds(133, 255, 105, 23);
		frame.getContentPane().add(btnSubmit);
		
		JButton btnCreateNew = new JButton("Create New");
		btnCreateNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				cnl.setVisible(true);
			}
		});
		btnCreateNew.setBounds(230, 255, 105, 23);
		frame.getContentPane().add(btnCreateNew);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(MainLogin.class.getResource("/edu/baylor/ecs/LoginPage/OSMIR.png")));
		lblNewLabel.setBounds(0, -83, 450, 361);
		frame.getContentPane().add(lblNewLabel);
	}
	
	public void setV(boolean flag) {
		frame.setVisible(flag);
	}
}
