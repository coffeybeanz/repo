package Java.Project.Prototypes;

public class GoodInjury extends Injury{
	
	private int daysOut;
	
	public GoodInjury() {
		this.daysOut = 0;
	}
	
	public int getDaysOut() {
		return this.daysOut;
	}
	
	public void setDaysOut(int n) {
		this.daysOut = n;
	}
}
