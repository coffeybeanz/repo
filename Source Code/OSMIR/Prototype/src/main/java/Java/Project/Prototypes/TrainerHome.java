package Java.Project.Prototypes;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class TrainerHome {

	private JFrame frame;
	private JTextField txtSearch;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public TrainerHome(String name) {
		initialize(name);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String name) {

		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.setBackground(Color.LIGHT_GRAY);
		mntmHome.setSelected(true);
		menuBar.add(mntmHome);

		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		menuBar.add(mntmAthletes);
		frame.getContentPane().setLayout(null);

		JList<?> list = new JList<Object>();
		list.setBackground(new Color(245, 245, 245));
		list.setBounds(127, 147, 460, 257);
		frame.getContentPane().add(list);

		txtSearch = new JTextField();
		txtSearch.setText("Search");
		txtSearch.setBounds(471, 13, 116, 22);
		frame.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);

		JLabel lblFirstName = new JLabel("First");
		lblFirstName.setBounds(50, 16, 83, 16);
		frame.getContentPane().add(lblFirstName);

		JLabel lblLast = new JLabel("Last");
		lblLast.setBounds(127, 16, 83, 16);
		frame.getContentPane().add(lblLast);

		JLabel lblResponsibleFor = new JLabel("Responsible For:");
		lblResponsibleFor.setBounds(127, 119, 108, 16);
		frame.getContentPane().add(lblResponsibleFor);
	}

	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}
}
