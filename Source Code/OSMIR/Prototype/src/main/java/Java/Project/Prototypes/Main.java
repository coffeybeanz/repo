package Java.Project.Prototypes;

import java.awt.EventQueue;

public class Main {

	/*  create instance of the login page */
	private static Login login = new Login();

	/* sets first screen's (login) visibility to true */
	public static void main(String[] args) {
		Injury gi = new GoodInjury();
		Injury bi = new BadInjury();
		gi.setName("GoodInjury");
		bi.setName("BadInjury");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					login.setVisibility(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
