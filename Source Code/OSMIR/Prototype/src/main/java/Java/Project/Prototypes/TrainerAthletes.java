package Java.Project.Prototypes;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;

public class TrainerAthletes {

	private JFrame frame;
	private JTextField txtSearch;

	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { TrainerAthletes window = new
	 * TrainerAthletes(); window.frame.setVisible(true); } catch (Exception e) {
	 * e.printStackTrace(); } } }); }
	 */

	public TrainerAthletes() {
		initialize();
	}

	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(false); // initially false

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		menuBar.add(mntmHome);

		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		menuBar.add(mntmAthletes);

		JMenuItem mntmTrainers = new JMenuItem("Trainers");
		menuBar.add(mntmTrainers);
		frame.getContentPane().setLayout(null);

		txtSearch = new JTextField();
		txtSearch.setText("Search");
		txtSearch.setBounds(532, 13, 116, 22);
		frame.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);

		JComboBox<?> comboBox = new JComboBox<Object>();
		comboBox.setBounds(191, 92, 319, 22);
		frame.getContentPane().add(comboBox);

		JList<?> list = new JList<Object>();
		list.setBackground(new Color(245, 245, 245));
		list.setBounds(191, 143, 319, 295);
		frame.getContentPane().add(list);

		JButton btnIrWatch = new JButton("IR & Watch List");
		btnIrWatch.setBounds(532, 238, 134, 25);
		frame.getContentPane().add(btnIrWatch);

		JLabel lblAthletes = new JLabel("Athletes:");
		lblAthletes.setBounds(191, 64, 61, 16);
		frame.getContentPane().add(lblAthletes);
	}
}
