package Java.Project.Prototypes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;

public class HeadTrainerAthletes {

	private JFrame frame;
	private JTextField txtSearch;
	private DefaultListModel<String> as = new DefaultListModel<String>();
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox = new JComboBox();

	public HeadTrainerAthletes() {
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* 
	 * make window and set UI
	 * displays list of athletes sorted by sports and lets user
	 * search for a player by name
	 * 
	 *  includes: 
	 *      menu bar
	 *      search bar - searches for player by full name
	 *      submit button - locates player if in database, otherwise
	 *          displays error dialogue
	 *      ir/watchlist button - displays list of all players on the
	 *          injured reserve/watchlist lists
	 *      new athlete button - adds a new athlete to the database
	 */
	@SuppressWarnings("unchecked")
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(false); // initially false

		/* creates and fills menu bar */
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerHome hth = new HeadTrainerHome();
				hth.setVisibility(true);
				as.clear();
				frame.dispose();
			}
		});
		menuBar.add(mntmHome);

		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		mntmAthletes.setBackground(Color.LIGHT_GRAY);
		mntmAthletes.setSelected(true);
		menuBar.add(mntmAthletes);

		JMenuItem mntmTrainers = new JMenuItem("Trainers");
		mntmTrainers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerTrainers htt = new HeadTrainerTrainers();
				htt.setVisibility(true);
				as.clear();
				frame.dispose();
			}
		});
		menuBar.add(mntmTrainers);
		frame.getContentPane().setLayout(null);

		/* fills options in the drop-down box */
		comboBox.setBounds(191, 92, 319, 22);
		comboBox.addItem("Football");
		comboBox.addItem("Baseball");
		comboBox.addItem("Basketball");
		comboBox.addItem("Soccer");
		comboBox.addItem("Volleyball");
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				as.clear();
				getAthletes(1);
			}
		});
		frame.getContentPane().add(comboBox);
		getAthletes(2);

		/* display athletes of selected sport */
		@SuppressWarnings("rawtypes")
		JList list = new JList();
		list.setBackground(new Color(245, 245, 245));
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBounds(191, 143, 319, 295);
		list.setModel(as);
		frame.getContentPane().add(list);

		/* opens player profile when selected from the list */
		MouseListener ml = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					String name = (String) list.getSelectedValue();
					String[] n = name.split(" ");
					File file = new File("Data.csv");
					try {
						Scanner scan = new Scanner(file);
						while (scan.hasNextLine()) {
							String line = scan.nextLine();
							String[] arr = line.split(",");
							if (arr[0].equals(n[0])) {
								HeadTrainerAthleteProf htap = new HeadTrainerAthleteProf(n[0]);
								htap.setVisibility(true);
								as.clear();
								frame.dispose();
							}
						}
						scan.close();
					} catch (FileNotFoundException ef) {
						ef.printStackTrace();
					}
				}
			}
		};
		list.addMouseListener(ml);

		/* new athlete button */
		JButton btnNewTrainer = new JButton("New Athlete");
		btnNewTrainer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewAthlete na = new NewAthlete();
				na.setVisibility(true);
				as.clear();
				frame.dispose();
			}
		});
		btnNewTrainer.setBounds(532, 238, 134, 25);
		frame.getContentPane().add(btnNewTrainer);

		/* injured reserve/watchlist button */
		JButton btnNewButton = new JButton("IR/Watchlist");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IRWatchlist irw = new IRWatchlist();
				irw.setVisibility(true);
			}
		});
		btnNewButton.setBounds(532, 188, 134, 23);
		frame.getContentPane().add(btnNewButton);

		JLabel lblAthletes = new JLabel("Athletes:");
		lblAthletes.setBounds(191, 64, 61, 16);
		frame.getContentPane().add(lblAthletes);

		/* search field */
		txtSearch = new JTextField();
		txtSearch.setText("Search");
		txtSearch.setBounds(461, 13, 116, 22);
		frame.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);

		/* searches for specified player in database, and if found displays
		 * their player profile, otherwise displays an error dialogue
		 */
		JButton submitBtn = new JButton("Submit");
		submitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean flag = false;
				File file = new File("Data.csv");
				try {
					@SuppressWarnings("resource")
					Scanner scan = new Scanner(file);
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						String[] arr = line.split(",");
						if (txtSearch.getText().equals(arr[0] + " " + arr[1]) && arr[4].equals("Athlete")) {
							flag = true;
							HeadTrainerAthleteProf htap = new HeadTrainerAthleteProf(arr[0]);
							htap.setVisibility(true);
							as.clear();
							frame.dispose();
						}

					}
				} catch (FileNotFoundException n) {
					n.printStackTrace();
				}
				if (!flag) { //display error dialogue
					ErrorDialogue ed = new ErrorDialogue("Athlete Name Not Found");
					ed.setVisibility(true);
				}
			}
		});
		submitBtn.setBounds(589, 12, 77, 25);
		frame.getContentPane().add(submitBtn);
	}

	/* 
	 * get the list of athletes in a specific sport and displays them
	 * in the center box 
	 */
	public void getAthletes(int num) {
		File file = new File("Data.csv");
		as.addElement("");
		try {
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (num == 1) {
					if (arr[4].equals("Athlete") && arr[8].equals(comboBox.getItemAt(comboBox.getSelectedIndex()))) {
						as.addElement(arr[0] + " " + arr[1] + " (" + arr[8] + ") - " + arr[9]);
					}
				} else {
					// as.removeAllElements();
					if (arr[4].equals("Athlete") && arr[8].equals("Football")) {
						as.addElement(arr[0] + " " + arr[1] + " (" + arr[8] + ") - " + arr[9]);
					}
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
