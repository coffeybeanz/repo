package Java.Project.Prototypes;

import java.util.ArrayList;

import Java.Project.Prototypes.Injury;

public class Athlete {

	private String firstName;
	private String lastName;
	private String sport;
	private String status;
	private String emergengyContact;
	private String emergengyContactNumber;
	private String medicalHistory;
	private ArrayList<Injury> injuries;

	public Athlete() {
		setFirstName("");
		setLastName("");
		setSport("");
		setStatus("");
		setEmergengyContact("");
		setEmergengyContactNumber("");
		setMedicalHistory("");
		injuries = new ArrayList<Injury>();
	}

	/* setters and getters */
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmergengyContact() {
		return emergengyContact;
	}

	public void setEmergengyContact(String emergengyContact) {
		this.emergengyContact = emergengyContact;
	}

	public String getEmergengyContactNumber() {
		return emergengyContactNumber;
	}

	public void setEmergengyContactNumber(String emergengyContactNumber) {
		this.emergengyContactNumber = emergengyContactNumber;
	}

	public String getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public ArrayList<Injury> getInjuries() {
		return injuries;
	}

	public void addInjury(Injury i) {
		injuries.add(i);
	}

}
