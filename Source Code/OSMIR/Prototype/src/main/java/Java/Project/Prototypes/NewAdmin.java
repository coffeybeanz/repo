package Java.Project.Prototypes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;

public class NewAdmin {

	private JFrame frame;
	private JTextField FN;
	private JTextField LN;
	private JTextField UN;
	private JPasswordField PWD;

	public NewAdmin() {
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}
	
	/* 
	 * make window and set UI
	 * provides input fields for the personal information of a
	 * new administrator-level user
	 * 
	 *  includes: 
	 *      submit button - saves the text entered in the fields to 
	 *      	the database
	 *      cancel button - exits the page and returns to the previous
	 *      	page without saving
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false);

		/* text fields for new admin's personal info */
		FN = new JTextField();
		FN.setBounds(285, 105, 269, 22);
		frame.getContentPane().add(FN);
		FN.setColumns(10);

		LN = new JTextField();
		LN.setBounds(285, 138, 269, 22);
		frame.getContentPane().add(LN);
		LN.setColumns(10);

		UN = new JTextField();
		UN.setBounds(285, 171, 269, 22);
		frame.getContentPane().add(UN);
		UN.setColumns(10);

		PWD = new JPasswordField();
		PWD.setBounds(285, 204, 269, 22);
		frame.getContentPane().add(PWD);

		/* submit button */
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				File file = new File("Data.csv");
				String out = "";
				Scanner scan;
				try {
					scan = new Scanner(file);
					while (scan.hasNextLine()) {
						out += scan.nextLine();
						out += "\n";
					}
					out += FN.getText() + ",";
					out += LN.getText() + ",";
					out += UN.getText() + ",";
					out += PWD.getText() + ",";
					out += "Admin\n";
					scan.close();
					FileWriter f;
					try {
						f = new FileWriter(file);
						f.write(out);
						f.flush();
						f.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				HeadTrainerHome hth = new HeadTrainerHome();
				hth.setVisibility(true);
				frame.dispose();
			}
		});
		btnSubmit.setBounds(180, 280, 97, 25);
		frame.getContentPane().add(btnSubmit);

		/* cancel button */
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerHome hth = new HeadTrainerHome();
				hth.setVisibility(true);
				frame.dispose();
			}
		});
		btnCancel.setBounds(333, 280, 97, 25);
		frame.getContentPane().add(btnCancel);

		/* labels for input fields */
		JLabel lblNewAdminUser = new JLabel("New Admin User");
		lblNewAdminUser.setBounds(246, 48, 136, 16);
		frame.getContentPane().add(lblNewAdminUser);

		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setBounds(127, 108, 85, 16);
		frame.getContentPane().add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setBounds(127, 141, 85, 16);
		frame.getContentPane().add(lblLastName);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(127, 174, 85, 16);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(127, 207, 85, 16);
		frame.getContentPane().add(lblPassword);

	}

}
