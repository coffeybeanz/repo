package Java.Project.Prototypes;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;

public class IRWatchlist {

	private JFrame frame;
	private JTextField txtSearch;
	private DefaultListModel<String> iw = new DefaultListModel<String>();

	public IRWatchlist() {
		getIRWatchs();
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* get list of players tagged injury reserved or on watchlist */
	public void getIRWatchs() {
		File file = new File("Data.csv");
		try {
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (arr[4].equals("Athlete") && !arr[9].equals("Cleared")) {
					iw.addElement(arr[0] + " " + arr[1] + " (" + arr[8] + ") - " + arr[9]);
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/* 
	 * make window and set UI
	 * displays list of players on the injury reserved list and 
	 * watchlist
	 * 
	 *  includes: 
	 *      menu bar
	 *      close button - closes window & returns to athlete list
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(false);
		frame.getContentPane().setLayout(null);

		/* list of injury reserved and watchlist athletes */
		JList<String> list = new JList<String>();
		list.setBackground(new Color(245, 245, 245));
		list.setBounds(191, 88, 319, 337);
		list.setModel(iw);
		frame.getContentPane().add(list);

		/* close button */
		JButton btnNewButton = new JButton("Close");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnNewButton.setBounds(290, 449, 123, 23);
		frame.getContentPane().add(btnNewButton);

		/* label for list */
		JLabel lblInjuryReserved = new JLabel("Injury Reserved & Watchlist Athletes:");
		lblInjuryReserved.setBounds(191, 60, 258, 16);
		frame.getContentPane().add(lblInjuryReserved);
	}

}
