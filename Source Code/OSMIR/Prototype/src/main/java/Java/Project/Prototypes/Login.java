package Java.Project.Prototypes;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Login {

	private JFrame frmOsmir;
	private JTextField textUsername;
	private JPasswordField textPassword;
	private String status = "";
	private String name = "";

	public Login() {
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frmOsmir.setVisible(flag);
	}
	
	/* 
	 * make window and set UI
	 * displays the group logo and contains space to enter username
	 * and password to log into system
	 * 
	 *  includes: 
	 *      logo 
	 * 		login button - in addition to validating provided username 
	 * 			and password, determines the type of account (ADMIN, TRAINER,
	 * 			or ATHLETE) and the proper opening pages
	 */
	private void initialize() {
		frmOsmir = new JFrame();
		frmOsmir.getContentPane().setBackground(Color.WHITE);
		frmOsmir.setTitle("O.S.M.I.R");
		frmOsmir.setResizable(false);
		frmOsmir.setBackground(Color.WHITE);
		frmOsmir.setBounds(100, 100, 720, 550);
		frmOsmir.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmOsmir.getContentPane().setLayout(null);
		frmOsmir.setVisible(false);

		/* username text field */
		textUsername = new JTextField();
		textUsername.setBounds(307, 266, 217, 22);
		frmOsmir.getContentPane().add(textUsername);
		textUsername.setColumns(10);

		/* password text field */
		textPassword = new JPasswordField();
		textPassword.setBounds(307, 300, 217, 22);
		frmOsmir.getContentPane().add(textPassword);

		/* login button */
		JButton btnSubmit = new JButton("Login");
		btnSubmit.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				File file = new File("Data.csv");
				try {
					Scanner scan = new Scanner(file);
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						String[] arr = line.split(",");
						if (textUsername.getText().equals(arr[2]) && textPassword.getText().equals(arr[3])) {
							name = arr[0];
							status = arr[4];
							break;
						}
					}
					scan.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				switch (status) {
				case "Admin":
					HeadTrainerHome hth = new HeadTrainerHome();
					hth.setVisibility(true);
					frmOsmir.dispose();
					break;
				case "Trainer":
					TrainerHome th = new TrainerHome(name);
					th.setVisibility(true);
					frmOsmir.dispose();
					break;
				case "Athlete":
					PlayerHome ph = new PlayerHome(name);
					ph.setVisibility(true);
					frmOsmir.dispose();
					break;
				default:
					textUsername.setText("");
					textPassword.setText("");
					break;
				}
			}
		});
		btnSubmit.setBounds(307, 356, 97, 25);
		frmOsmir.getContentPane().add(btnSubmit);

		/* header image of logo */
		JLabel lblOsmir = new JLabel("");
		lblOsmir.setIcon(new ImageIcon(Login.class.getResource("OSMIR.png")));
		lblOsmir.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		lblOsmir.setBounds(137, 79, 500, 132);
		frmOsmir.getContentPane().add(lblOsmir);

		/* username and password labels */
		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setBounds(209, 269, 86, 16);
		frmOsmir.getContentPane().add(lblNewLabel);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(209, 303, 86, 16);
		frmOsmir.getContentPane().add(lblPassword);
	}
}
