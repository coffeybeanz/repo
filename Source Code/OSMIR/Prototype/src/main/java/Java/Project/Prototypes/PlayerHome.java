package Java.Project.Prototypes;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class PlayerHome {

	private JFrame frame;
	private JTextPane EC;
	private JTextPane ECN;

	public PlayerHome(String name) {
		String[] info = getInfo(name);
		initialize(info);
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* gets the players information from the database, based 
	 * on the name provided as a variable */
	public String[] getInfo(String name) {
		File file = new File("Data.csv");
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (arr[0].equals(name)) {
					return arr;
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/* 
	 * make window and set UI
	 * the starting page after an athlete logs in; it automaticallly loads
	 * to their profile, and the only other page they have access to is their
	 * medical information (that they cannot edit)
	 * 
	 *  includes: 
	 *      cancel button - exits the application
	 */
	private void initialize(String[] info) {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false); // initially false

		/* menu bar */
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.setBackground(Color.LIGHT_GRAY);
		mntmHome.setSelected(true);
		menuBar.add(mntmHome);

		JMenuItem mntmMedicalInfo = new JMenuItem("Medical Info");
		mntmMedicalInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PlayerMedStuff pms = new PlayerMedStuff(info[0],1);
				pms.setVisibility(true);
				frame.dispose();
			}
		});
		menuBar.add(mntmMedicalInfo);

		/* text panes for personal information */
		JTextPane FN = new JTextPane();
		FN.setBackground(new Color(245, 245, 245));
		FN.setEditable(false);
		FN.setText(info[0]);
		FN.setBounds(45, 66, 123, 22);
		frame.getContentPane().add(FN);

		JTextPane LN = new JTextPane();
		LN.setBackground(new Color(245, 245, 245));
		LN.setEditable(false);
		LN.setText(info[1]);
		LN.setBounds(219, 66, 123, 22);
		frame.getContentPane().add(LN);

		JTextPane SPORT = new JTextPane();
		SPORT.setBackground(new Color(245, 245, 245));
		SPORT.setEditable(false);
		SPORT.setText(info[8]);
		SPORT.setBounds(504, 150, 123, 22);
		frame.getContentPane().add(SPORT);

		JTextPane STAT = new JTextPane();
		STAT.setBackground(new Color(245, 245, 245));
		STAT.setEditable(false);
		STAT.setText(info[9]);
		STAT.setBounds(504, 66, 123, 22);
		frame.getContentPane().add(STAT);

		/* close button */
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnClose.setBounds(325, 422, 97, 25);
		frame.getContentPane().add(btnClose);

		EC = new JTextPane();
		EC.setBackground(new Color(245, 245, 245));
		EC.setEditable(false);
		EC.setText(info[6]);
		EC.setBounds(254, 216, 373, 22);
		frame.getContentPane().add(EC);

		ECN = new JTextPane();
		ECN.setBackground(new Color(245, 245, 245));
		ECN.setEditable(false);
		ECN.setText(info[7]);
		ECN.setBounds(254, 251, 373, 22);
		frame.getContentPane().add(ECN);

		JTextPane E = new JTextPane();
		E.setBackground(new Color(245, 245, 245));
		E.setEditable(false);
		E.setText(info[5]);
		E.setBounds(45, 150, 414, 22);
		frame.getContentPane().add(E);

		/* labels for text fields */
		JLabel lblNewLabel = new JLabel("First Name:");
		lblNewLabel.setBounds(45, 38, 123, 16);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setBounds(219, 38, 123, 16);
		frame.getContentPane().add(lblLastName);

		JLabel lblEmailAddress = new JLabel("Email Address:");
		lblEmailAddress.setBounds(45, 129, 140, 16);
		frame.getContentPane().add(lblEmailAddress);

		JLabel lblEmergencyContact = new JLabel("Emergency Contact Name:");
		lblEmergencyContact.setBounds(45, 216, 175, 16);
		frame.getContentPane().add(lblEmergencyContact);

		JLabel lblEmergencyContactNumber = new JLabel("Emergency Contact Number:");
		lblEmergencyContactNumber.setBounds(45, 257, 197, 16);
		frame.getContentPane().add(lblEmergencyContactNumber);

		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(504, 38, 123, 16);
		frame.getContentPane().add(lblStatus);

		JLabel lblSport = new JLabel("Sport:");
		lblSport.setBounds(504, 122, 123, 16);
		frame.getContentPane().add(lblSport);
		
		JLabel lblTrainerName = new JLabel("Trainer Name:");
		lblTrainerName.setBounds(123, 339, 97, 16);
		frame.getContentPane().add(lblTrainerName);
		
		JTextPane trainerText = new JTextPane();
		trainerText.setText(info[11]);
		trainerText.setEditable(false);
		trainerText.setBackground(new Color(245, 245, 245));
		trainerText.setBounds(254, 339, 373, 22);
		frame.getContentPane().add(trainerText);
	}
}
