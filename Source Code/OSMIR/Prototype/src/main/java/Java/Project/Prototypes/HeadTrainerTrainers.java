package Java.Project.Prototypes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;

public class HeadTrainerTrainers {

	private JFrame frame;
	private JTextField txtSearch;
	private DefaultListModel<String> ts = new DefaultListModel<String>();
	private JComboBox<String> comboBox = new JComboBox<String>();

	public HeadTrainerTrainers() {
		getTrainers();
		initialize();
	}

	/* get list of trainers sorted by the sport they supervise */
	public void getTrainers() {
		File file = new File("Data.csv");
		ts.addElement("");
		try {
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (arr[4].equals("Trainer")) {
					ts.addElement(arr[0] + " " + arr[1] + " (" + arr[6] + ")");
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}
	
	/* 
	 * make window and set UI
	 * displays list of trainers sorted by sports and lets user
	 * search for a person by name
	 * 
	 *  includes: 
	 *      menu bar
	 *      search bar - searches for player by full name
	 *      submit button - locates player if in database, otherwise
	 *          displays error dialogue
	 *      ir/watchlist button - displays list of all players on the
	 *          injured reserve/watchlist lists
	 *      new athlete button - adds a new athlete to the database
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(false); // initially false

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerHome hth = new HeadTrainerHome();
				hth.setVisibility(true);
				frame.dispose();
			}
		});
		menuBar.add(mntmHome);

		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		mntmAthletes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerAthletes hta = new HeadTrainerAthletes();
				hta.setVisibility(true);
				frame.dispose();
			}
		});
		menuBar.add(mntmAthletes);

		JMenuItem mntmTrainers = new JMenuItem("Trainers");
		mntmTrainers.setBackground(Color.LIGHT_GRAY);
		mntmTrainers.setSelected(true);
		menuBar.add(mntmTrainers);
		frame.getContentPane().setLayout(null);

		/* search bar for trainers */
		txtSearch = new JTextField();
		txtSearch.setText("Search");
		txtSearch.setBounds(471, 13, 116, 22);
		frame.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);

		/* fills options in the drop-down box */
		comboBox.setBounds(191, 92, 319, 22);
		comboBox.addItem("Football");
		comboBox.addItem("Baseball");
		comboBox.addItem("Basketball");
		comboBox.addItem("Soccer");
		comboBox.addItem("Volleyball");
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ts.clear();
				getAthletes(1);
			}
		});
		frame.getContentPane().add(comboBox);
		getAthletes(2);
		
		/* display trainers of selected sport */
		JList<String> list = new JList<String>();
		list.setBackground(new Color(245, 245, 245));
		list.setBounds(191, 143, 319, 295);
		list.setModel(ts);
		frame.getContentPane().add(list);

		/* new trainer button */
		JButton btnNewTrainer = new JButton("New Trainer");
		btnNewTrainer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewTrainer nt = new NewTrainer();
				nt.setVisibility(true);
				frame.dispose();
			}
		});
		btnNewTrainer.setBounds(532, 238, 134, 25);
		frame.getContentPane().add(btnNewTrainer);

		JLabel lblTrainers = new JLabel("Trainers:");
		lblTrainers.setBounds(191, 64, 61, 16);
		frame.getContentPane().add(lblTrainers);

		/* searches for specified trainer in database, and if found displays
		 * their trainer profile, otherwise displays an error dialogue
		 */
		JButton submitBtn = new JButton("Submit");
		submitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean flag = false;
				File file = new File("Data.csv");
				try {
					@SuppressWarnings("resource")
					Scanner scan = new Scanner(file);
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						String[] arr = line.split(",");
						if (txtSearch.getText().equals(arr[0] + " " + arr[1]) && arr[4].equals("Trainer")) {
							flag = true;
							ErrorDialogue erd = new ErrorDialogue("The Trainer Exists");
							erd.setVisibility(true);
						}

					}
				} catch (FileNotFoundException n) {
					n.printStackTrace();
				}
				if (!flag) {
					ErrorDialogue ed = new ErrorDialogue("Trainer Name Not Found");
					ed.setVisibility(true);
				}

			}
		});
		submitBtn.setBounds(592, 12, 74, 25);
		frame.getContentPane().add(submitBtn);
	}

	public void getAthletes(int num) {
		File file = new File("Data.csv");
		ts.addElement("");
		try {
			Scanner scan = new Scanner(file);
			ts.clear();
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (num == 1) {
					if (arr[4].equals("Trainer") && arr[6].equals(comboBox.getItemAt(comboBox.getSelectedIndex()))) {
						ts.addElement(arr[0] + " " + arr[1] + " (" + arr[6] + ")");
					}
				} else {
					// as.removeAllElements();
					if (arr[4].equals("Trainer") && arr[6].equals("Football")) {
						ts.addElement(arr[0] + " " + arr[1] + " (" + arr[6] + ")");
					}
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
