package Java.Project.Prototypes;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class NewAthlete {

	private JFrame frame;
	private JTextField FN;
	private JTextField LN;
	private JTextField E;
	private JPasswordField PWD;
	private JTextField EC;
	private JTextField ECN;
	private JTextField UN;

	public NewAthlete() {
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}
	
	/* 
	 * make window and set UI
	 * provides input fields for the personal information of a
	 * new athlete-level user
	 * 
	 *  includes: 
	 *      submit button - saves the text entered in the fields to 
	 *      	the database
	 *      cancel button - exits the page and returns to the previous
	 *      	page without saving
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false);

		/* text fields for the new athlete's personal info */
		FN = new JTextField();
		FN.setBounds(308, 61, 269, 22);
		frame.getContentPane().add(FN);
		FN.setColumns(10);

		UN = new JTextField();
		UN.setColumns(10);
		UN.setBounds(308, 131, 269, 22);
		frame.getContentPane().add(UN);

		LN = new JTextField();
		LN.setBounds(308, 96, 269, 22);
		frame.getContentPane().add(LN);
		LN.setColumns(10);

		E = new JTextField();
		E.setBounds(308, 201, 269, 22);
		frame.getContentPane().add(E);
		E.setColumns(10);

		PWD = new JPasswordField();
		PWD.setBounds(308, 166, 269, 22);
		frame.getContentPane().add(PWD);

		EC = new JTextField();
		EC.setBounds(307, 236, 270, 20);
		frame.getContentPane().add(EC);
		EC.setColumns(10);

		ECN = new JTextField();
		ECN.setColumns(10);
		ECN.setBounds(307, 271, 270, 20);
		frame.getContentPane().add(ECN);

		/* options to select sport from */
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(308, 306, 269, 22);
		comboBox.addItem("Football");
		comboBox.addItem("Baseball");
		comboBox.addItem("Basketball");
		comboBox.addItem("Soccer");
		comboBox.addItem("Volleyball");
		frame.getContentPane().add(comboBox);

		/* options to select athlete status from */
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setBounds(308, 341, 269, 22);
		comboBox_1.addItem("Cleared");
		comboBox_1.addItem("Watchlist");
		comboBox_1.addItem("Injury Reserved");
		frame.getContentPane().add(comboBox_1);

		JTextArea textArea = new JTextArea();
		textArea.setBounds(308, 405, 269, 41);
		frame.getContentPane().add(textArea);
		
		/* set assigned trainer to athlete */
		JComboBox<String> trainerComboBox = new JComboBox<String>();
		trainerComboBox.setBounds(308, 379, 269, 22);
		File file = new File("Data.csv");
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(file);
			while(scan.hasNextLine()) {
				String line = scan.nextLine();
				String arr[] = line.split(",");
				if(arr[4].equals("Trainer")) {
					trainerComboBox.addItem(arr[0] + " " + arr[1]);
				}
			}
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		frame.getContentPane().add(trainerComboBox);

		/* submit button */
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				File file = new File("Data.csv");
				String out = "";
				Scanner scan;
				try {
					scan = new Scanner(file);
					while (scan.hasNextLine()) {
						out += scan.nextLine();
						out += "\n";
					}
					out += FN.getText() + ",";
					out += LN.getText() + ",";
					out += UN.getText() + ",";
					out += PWD.getText() + ",";
					out += "Athlete,";
					out += E.getText() + ",";
					out += EC.getText() + ",";
					out += ECN.getText() + ",";
					out += comboBox.getItemAt(comboBox.getSelectedIndex()) + ",";
					out += comboBox_1.getItemAt(comboBox_1.getSelectedIndex()) + ",";
					out += textArea.getText() + ",";
					out += trainerComboBox.getItemAt(trainerComboBox.getSelectedIndex()) + "\n";
					scan.close();
					FileWriter f;
					try {
						f = new FileWriter(file);
						f.write(out);
						f.flush();
						f.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				HeadTrainerAthletes hta = new HeadTrainerAthletes();
				hta.setVisibility(true);
				frame.dispose();
			}
		});
		btnSubmit.setBounds(201, 454, 97, 25);
		frame.getContentPane().add(btnSubmit);

		/* cancel button */
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerAthletes hta = new HeadTrainerAthletes();
				hta.setVisibility(true);
				frame.dispose();
			}
		});
		btnCancel.setBounds(366, 454, 97, 25);
		frame.getContentPane().add(btnCancel);

		/* labels for input fields */
		JLabel lblNewLabel = new JLabel("First Name:");
		lblNewLabel.setBounds(118, 64, 112, 16);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setBounds(118, 99, 152, 16);
		frame.getContentPane().add(lblLastName);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(118, 134, 112, 16);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(118, 169, 112, 16);
		frame.getContentPane().add(lblPassword);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(118, 204, 85, 16);
		frame.getContentPane().add(lblEmail);

		JLabel lblEmergencyContact = new JLabel("Emergency Contact:");
		lblEmergencyContact.setBounds(118, 238, 136, 16);
		frame.getContentPane().add(lblEmergencyContact);

		JLabel lblEmergencyContactNumber = new JLabel("Emergency Contact Number:");
		lblEmergencyContactNumber.setBounds(118, 273, 189, 16);
		frame.getContentPane().add(lblEmergencyContactNumber);

		JLabel lblSport = new JLabel("Sport:");
		lblSport.setBounds(118, 308, 97, 16);
		frame.getContentPane().add(lblSport);

		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(118, 343, 206, 16);
		frame.getContentPane().add(lblStatus);

		JLabel lblMedicalHistory = new JLabel("Medical History:");
		lblMedicalHistory.setBounds(118, 408, 136, 16);
		frame.getContentPane().add(lblMedicalHistory);

		JLabel lblNewAthlete = new JLabel("New Athlete");
		lblNewAthlete.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewAthlete.setBounds(284, 33, 91, 16);
		frame.getContentPane().add(lblNewAthlete);
		
		JLabel lblTrainer = new JLabel("Trainer:");
		lblTrainer.setBounds(118, 379, 56, 16);
		frame.getContentPane().add(lblTrainer);
	}
}
