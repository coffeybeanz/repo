package Java.Project.Prototypes;

import java.util.ArrayList;

public class Injury {
	private String name;
	private String description;
	private ArrayList<String> treatments;

	public Injury() {
		setName("");
		setDescription("");
		treatments = new ArrayList<String>();
		created = true;
		singleton = "Hello, World!";
	}

	/* setters and getters */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<String> getTreatments() {
		return treatments;
	}

	public void addTreatment(String t) {
		treatments.add(t);
	}
	
	boolean created = false;
	String singleton = null;
	
	public String getSingleton() {
		if ( created ) {
			return singleton;
		}
		
		created = true;
		singleton = "Hello, boiz & gurlz";
		return singleton;
	}
	
	

}
