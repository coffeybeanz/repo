package Java.Project.Prototypes;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;

public class TrainerInjury {

	private JFrame frame;

	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { TrainerInjury window = new
	 * TrainerInjury(); window.frame.setVisible(true); } catch (Exception e) {
	 * e.printStackTrace(); } } }); }
	 */
	public TrainerInjury() {
		initialize();
	}

	/**
	 * Set visibility
	 */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(false); // initially false

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		menuBar.add(mntmHome);

		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		menuBar.add(mntmAthletes);

		JMenuItem mntmTrainers = new JMenuItem("Trainers");
		menuBar.add(mntmTrainers);
		frame.getContentPane().setLayout(null);

		JTextPane txtpnFirstName = new JTextPane();
		txtpnFirstName.setBounds(0, 0, 190, 22);
		txtpnFirstName.setText("First Name");
		frame.getContentPane().add(txtpnFirstName);

		JTextPane txtpnLastName = new JTextPane();
		txtpnLastName.setBounds(190, 0, 188, 22);
		txtpnLastName.setText("Last Name");
		frame.getContentPane().add(txtpnLastName);

		JTextPane txtpnSport = new JTextPane();
		txtpnSport.setBounds(378, 0, 159, 22);
		txtpnSport.setText("Sport");
		frame.getContentPane().add(txtpnSport);

		JTextPane txtpnStatus = new JTextPane();
		txtpnStatus.setBounds(537, 0, 164, 22);
		txtpnStatus.setText("Status");
		frame.getContentPane().add(txtpnStatus);

		JTextPane txtpnNewInjury = new JTextPane();
		txtpnNewInjury.setText("New Injury");
		txtpnNewInjury.setBounds(0, 48, 701, 22);
		frame.getContentPane().add(txtpnNewInjury);

		JTextArea txtrtext = new JTextArea();
		txtrtext.setBackground(new Color(245, 245, 245));
		txtrtext.setText("(Text)");
		txtrtext.setBounds(181, 165, 387, 78);
		frame.getContentPane().add(txtrtext);

		JTextArea txtrtext_1 = new JTextArea();
		txtrtext_1.setBackground(new Color(245, 245, 245));
		txtrtext_1.setText("(Text)");
		txtrtext_1.setBounds(181, 83, 509, 35);
		frame.getContentPane().add(txtrtext_1);

		JTextArea txtrtext_2 = new JTextArea();
		txtrtext_2.setBackground(new Color(245, 245, 245));
		txtrtext_2.setText("(Text)");
		txtrtext_2.setBounds(181, 270, 387, 78);
		frame.getContentPane().add(txtrtext_2);

		JTextArea txtrtext_3 = new JTextArea();
		txtrtext_3.setBackground(new Color(245, 245, 245));
		txtrtext_3.setText("(Text)");
		txtrtext_3.setBounds(181, 377, 387, 78);
		frame.getContentPane().add(txtrtext_3);

		JButton btnAddNew = new JButton("Add New");
		btnAddNew.setBounds(593, 189, 97, 25);
		frame.getContentPane().add(btnAddNew);

		JButton btnAddNew_1 = new JButton("Add New");
		btnAddNew_1.setBounds(593, 306, 97, 25);
		frame.getContentPane().add(btnAddNew_1);

		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(10, 82, 97, 16);
		frame.getContentPane().add(lblDescription);

		JLabel lblTreatment = new JLabel("Treatment:");
		lblTreatment.setBounds(10, 165, 82, 16);
		frame.getContentPane().add(lblTreatment);

		JLabel lblNewLabel = new JLabel("Notes:");
		lblNewLabel.setBounds(10, 270, 61, 16);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblPastTreatments = new JLabel("Past Treatments:");
		lblPastTreatments.setBounds(10, 377, 110, 16);
		frame.getContentPane().add(lblPastTreatments);
	}
}
