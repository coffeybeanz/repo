package Java.Project.Prototypes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.Color;

public class HeadTrainerHome {

	private JFrame frame;
	public HeadTrainerHome() {
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* 
	 * make window and set UI
	 * the opening window when signing in with an administrator
	 * account
	 * 
	 * includes: 
	 *     menu bar
	 *     new user button - creates a new user, type specified by 
	 *         admin
	 *     close button - closes window
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(false); // initially false

		/* creates and fills menu bar */
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.setBackground(Color.LIGHT_GRAY);
		mntmHome.setSelected(true);
		menuBar.add(mntmHome);

		JMenuItem mntmAthletes = new JMenuItem("Athletes");
		mntmAthletes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerAthletes hta = new HeadTrainerAthletes();
				hta.setVisibility(true);
				frame.dispose();
			}
		});
		menuBar.add(mntmAthletes);

		JMenuItem mntmTrainers = new JMenuItem("Trainers");
		mntmTrainers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerTrainers htt = new HeadTrainerTrainers();
				htt.setVisibility(true);
				frame.dispose();
			}
		});
		menuBar.add(mntmTrainers);
		frame.getContentPane().setLayout(null);

		/* displays welcome message on home page */
		JTextPane txtpnWelcome = new JTextPane();
		txtpnWelcome.setBackground(new Color(245, 245, 245));
		txtpnWelcome.setEditable(false);
		txtpnWelcome.setText("WELCOME");
		txtpnWelcome.setBounds(111, 80, 508, 77);
		frame.getContentPane().add(txtpnWelcome);

		/* create new user button */
		JButton btnNewUser = new JButton("Create New User");
		btnNewUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NewAdmin na = new NewAdmin();
				na.setVisibility(true);
				frame.dispose();
			}
		});
		btnNewUser.setBounds(190, 270, 158, 23);
		frame.getContentPane().add(btnNewUser);

		/* close button */
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnClose.setBounds(419, 265, 105, 29);
		frame.getContentPane().add(btnClose);
	}
}
