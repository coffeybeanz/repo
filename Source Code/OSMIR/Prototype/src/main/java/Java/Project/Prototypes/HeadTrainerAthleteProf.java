package Java.Project.Prototypes;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.JLabel;

public class HeadTrainerAthleteProf {

	private JFrame frame;
	private JTextPane EC;
	private JTextPane ECN;
	private String name;
	private JComboBox<String> comboBox;

	public HeadTrainerAthleteProf(String name) {
		String[] info = getInfo(name);
		initialize(info);
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* grabs the information of the person whose name
	 * is passed as the variable */
	public String[] getInfo(String name) {
		File file = new File("Data.csv");
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (arr[0].equals(name) && arr[4].equals("Athlete")) {
					return arr;
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/* 
	 * make window and set UI
	 * displays previously selected athlete's player profile, with 
	 * personal information 
	 * 
	 * includes: 
	 *     menu bar 
	 *     close button - closes window
	 *     new injury button - opens new injury window so trainer can 
	 *         add a new injury to the player's profile
	 *     save button - writes player's information to database
	 */
	@SuppressWarnings("unchecked")
	private void initialize(String[] info) {
		frame = new JFrame();
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false); // initially false

		/* creates and fills menu bar */
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.setBackground(Color.LIGHT_GRAY);
		mntmHome.setSelected(true);
		menuBar.add(mntmHome);

		JMenuItem mntmMedicalInfo = new JMenuItem("Medical Info");
		mntmMedicalInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PlayerMedStuff pms = new PlayerMedStuff(info[0],0);
				pms.setVisibility(true);
				frame.dispose();
			}
		});
		menuBar.add(mntmMedicalInfo);

		/* fills player personal info */
		JTextPane FN = new JTextPane();
		FN.setEditable(false);
		FN.setText(info[0]);
		FN.setBounds(45, 66, 123, 22);
		frame.getContentPane().add(FN);

		JTextPane LN = new JTextPane();
		LN.setEditable(false);
		LN.setText(info[1]);
		LN.setBounds(219, 66, 123, 22);
		frame.getContentPane().add(LN);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(504, 150, 123, 22);
		comboBox.addItem("Football");
		comboBox.addItem("Baseball");
		comboBox.addItem("Basketball");
		comboBox.addItem("Soccer");
		comboBox.addItem("Volleyball");
		comboBox.setSelectedItem(info[8]);
		frame.getContentPane().add(comboBox);

		JTextPane STAT = new JTextPane();
		STAT.setText(info[9]);
		STAT.setBounds(504, 66, 123, 22);
		frame.getContentPane().add(STAT);

		/* close button */
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerAthletes hta = new HeadTrainerAthletes();
				hta.setVisibility(true);
				frame.dispose();
			}
		});
		btnClose.setBounds(47, 420, 97, 25);
		frame.getContentPane().add(btnClose);

		/* fill information for player emergency contact & 
		 * emergency contact number */
		EC = new JTextPane();
		EC.setText(info[6]);
		EC.setBounds(254, 216, 373, 22);
		frame.getContentPane().add(EC);

		ECN = new JTextPane();
		ECN.setText(info[7]);
		ECN.setBounds(254, 251, 373, 22);
		frame.getContentPane().add(ECN);

		JTextPane E = new JTextPane();
		E.setText(info[5]);
		E.setBounds(45, 150, 414, 22);
		frame.getContentPane().add(E);

		/* new injury button */
		JButton btnAddInjury = new JButton("New Injury");
		btnAddInjury.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				name = info[0] + "_" + info[1];
				NewInjury ni = new NewInjury(name);
				ni.setVisibility(true);
			}
		});
		btnAddInjury.setBounds(464, 420, 97, 25);
		frame.getContentPane().add(btnAddInjury);

		/* save button */
		JButton btnEdit = new JButton("Save");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!info[5].equals(E.getText())) {
					info[5] = E.getText();
				}
				if (!info[6].equals(EC.getText())) {
					info[6] = EC.getText();
				}
				if (!info[7].equals(ECN.getText())) {
					info[7] = ECN.getText();
				}
//				if (!info[8].equals(SPORT.getText())) {
//					info[8] = SPORT.getText();
//				}
				if(!info[8].equals(comboBox.getItemAt(comboBox.getSelectedIndex()))) {
					info[8] = (String)comboBox.getItemAt(comboBox.getSelectedIndex());
				}
				if (!info[9].equals(STAT.getText())) {
					info[9] = STAT.getText();
				}
				File file = new File("Data.csv");
				try {
					Scanner scan = new Scanner(file);
					String out1 = "";
					String out2 = "";
					boolean flag = false;
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						String arr[] = line.split(",");
						if (!arr[0].equals(info[0]) && !flag) {
							out1 += line;
							out1 += "\n";
						} else {
							flag = true;
						}
						if (!arr[0].equals(info[0]) && flag) {
							out2 += line;
							out2 += "\n";
						}
					}
					scan.close();
					FileWriter fw = new FileWriter(file);
					String out3 = info[0] + "," + info[1] + "," + info[2] + "," + info[3] + "," + info[4] + ","
							+ info[5] + "," + info[6] + "," + info[7] + "," + info[8] + "," + info[9] + "," + info[10]
							+ "," + info[11] + "\n";
					out1 += out3;
					out1 += out2;
					fw.write(out1);
					fw.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnEdit.setBounds(267, 420, 97, 25);
		frame.getContentPane().add(btnEdit);

		/* display labels for player information */
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setBounds(45, 38, 99, 16);
		frame.getContentPane().add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setBounds(219, 38, 99, 16);
		frame.getContentPane().add(lblLastName);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(45, 123, 99, 16);
		frame.getContentPane().add(lblEmail);

		JLabel lblSport = new JLabel("Status:");
		lblSport.setBounds(504, 38, 99, 16);
		frame.getContentPane().add(lblSport);

		JLabel lblSport_1 = new JLabel("Sport:");
		lblSport_1.setBounds(504, 123, 99, 16);
		frame.getContentPane().add(lblSport_1);

		JLabel lblEmergencyContact = new JLabel("Emergency Contact: ");
		lblEmergencyContact.setBounds(45, 216, 154, 16);
		frame.getContentPane().add(lblEmergencyContact);

		JLabel lblEmergencyContactNumber = new JLabel("Emergency Contact Number: ");
		lblEmergencyContactNumber.setBounds(45, 251, 197, 16);
		frame.getContentPane().add(lblEmergencyContactNumber);
		
		JLabel lblTrainerName = new JLabel("Trainer Name:");
		lblTrainerName.setBounds(125, 302, 97, 16);
		frame.getContentPane().add(lblTrainerName);
		
		JTextPane trainerText = new JTextPane();
		trainerText.setEditable(false);
		trainerText.setText(info[11]);
		trainerText.setBounds(254, 296, 373, 22);
		frame.getContentPane().add(trainerText);
	}
}
