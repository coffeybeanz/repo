package Java.Project.Prototypes;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class BadInjury extends Injury {
	
	private boolean hospital;
	
	public BadInjury() {
		this.hospital = false;
	}
	
	public boolean getHospital() {
		return this.hospital;
	}
	
	public void setHospital(boolean flag) {
		this.hospital = flag;
	}

}
