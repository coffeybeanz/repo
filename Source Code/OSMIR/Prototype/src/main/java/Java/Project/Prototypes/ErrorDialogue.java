package Java.Project.Prototypes;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ErrorDialogue {

	private JFrame frame;
	private String message = "";

	public ErrorDialogue(String m) {
		this.message = m;
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* make window and set UI
	 * 
	 * includes: 
	 *     close button - closes window
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel message = new JLabel("");
		message.setFont(new Font("Times New Roman", Font.BOLD, 14));
		message.setBounds(64, 40, 306, 94);
		message.setText(this.message);
		frame.getContentPane().add(message);

		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		closeBtn.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		closeBtn.setBounds(167, 169, 97, 25);
		frame.getContentPane().add(closeBtn);
	}
}
