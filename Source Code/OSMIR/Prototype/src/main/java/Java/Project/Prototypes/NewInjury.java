package Java.Project.Prototypes;


import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class NewInjury implements ActionListener {

	private JFrame frame;
	private JTextField txttext;
	private JTextField textField;
	private JTextField input;
	private String name;

	public NewInjury(String n) {
		name = n;
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}
	
	/* 
	 * make window and set UI
	 * provides input fields for information on a new injury for an
	 * athlete
	 * 
	 *  includes: 
	 *      submit button - saves the text entered in the fields to 
	 *      	the database
	 *      cancel button - exits the page and returns to the previous
	 *      	page without saving
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false);

		/* text fields for injury input */
		txttext = new JTextField();
		txttext.setBounds(308, 86, 289, 22);
		frame.getContentPane().add(txttext);
		txttext.setColumns(10);
		txttext.addActionListener(this);

		input = new JTextField();
		input.setBounds(95, 156, 502, 70);
		frame.getContentPane().add(input);
		input.setColumns(10);
		input.addActionListener(this);

		textField = new JTextField();
		textField.setBounds(95, 274, 502, 103);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		textField.addActionListener(this);

		/* labels for text fields */
		JLabel lblNewLabel = new JLabel("New Injury");
		lblNewLabel.setBounds(314, 45, 97, 16);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblInjuryName = new JLabel("Injury Name:");
		lblInjuryName.setBounds(95, 89, 97, 16);
		frame.getContentPane().add(lblInjuryName);

		JLabel lblInjuryDescription = new JLabel("Injury Description:");
		lblInjuryDescription.setBounds(95, 128, 138, 16);
		frame.getContentPane().add(lblInjuryDescription);

		JLabel lblInjuryTreatment = new JLabel("Injury Treatment:");
		lblInjuryTreatment.setBounds(95, 246, 138, 16);
		frame.getContentPane().add(lblInjuryTreatment);

		/* submit button */
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				String n = txttext.getText() + "\n";
				String description = input.getText() + "\n";
				String treatment = textField.getText();
				name += ".txt"; 
				File newFile = new File(name);
				try {
					newFile.createNewFile();
					@SuppressWarnings("resource")
					FileOutputStream oFile = new FileOutputStream(newFile);
					oFile.write((n + description + treatment).getBytes());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnSubmit.setBounds(184, 441, 97, 25);
		frame.getContentPane().add(btnSubmit);

		/* cancel button */
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnCancel.setBounds(380, 441, 97, 25);
		frame.getContentPane().add(btnCancel);

	}

	/* writes the new injury to the database, associating it with the player */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		frame.dispose();
		String n = txttext.getText() + "\n";
		String description = input.getText() + "\n";
		String treatment = textField.getText();
		name += ".txt";
		File newFile = new File(name);
		try {
			newFile.createNewFile();
			@SuppressWarnings("resource")
			FileOutputStream oFile = new FileOutputStream(newFile);
			oFile.write((n + description + treatment).getBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
