package Java.Project.Prototypes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.Color;
import javax.swing.JLabel;

public class PlayerMedStuff {

	private JFrame frame;
	private int status;
	
	public PlayerMedStuff(String name, int s) {
		String[] info = getInfo(name);
		this.status = s;
		initialize(info);
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}

	/* gets the player's medical information from the 
	 * database based off 
	 */
	public String[] getInfo(String name) {
		File file = new File("Data.csv");
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(file);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] arr = line.split(",");
				if (arr[0].equals(name)) {
					return arr;
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}


	/* 
	 * make window and set UI
	 * displays the player's medical information and current
	 * injury, with the recommended treatment
	 * 
	 *  includes: 
	 *      cancel button - exits the application
	 */
	private void initialize(String[] info) {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false); // initially false

		/* menu bar */
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenuItem mntmHome = new JMenuItem("Home");
		System.out.println(this.status);
		if(this.status == 0) {
			System.out.println("Here");
			mntmHome.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					HeadTrainerAthleteProf htap = new HeadTrainerAthleteProf(info[0]);
					htap.setVisibility(true);
					frame.dispose();
				}
			});
		}
		else {
			mntmHome.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PlayerHome ph = new PlayerHome(info[0]);
					ph.setVisibility(true);
					frame.dispose();
				}
			});
		}
		menuBar.add(mntmHome);

		JMenuItem mntmMedicalInfo = new JMenuItem("Medical Info");
		mntmMedicalInfo.setBackground(Color.LIGHT_GRAY);
		mntmMedicalInfo.setSelected(true);
		menuBar.add(mntmMedicalInfo);

		/* grabs medical information */
		String input[] = new String[3];
		try {
			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new FileReader(info[0] + "_" + info[1] + ".txt"));
			String line;
			int i = 0;
			while ((line = reader.readLine()) != null) {
				input[i] = line;
				i++;
			}
		} catch (IOException e1) {
			input[0] = "";
			input[1] = "";
			input[2] = "";
			//e1.printStackTrace();
		}

		/* text areas to display medical information */
		JTextArea txtrtext = new JTextArea();
		txtrtext.setBackground(new Color(245, 245, 245));
		txtrtext.setEditable(false);
		txtrtext.setText(input[0]);
		txtrtext.setBounds(221, 25, 431, 49);
		frame.getContentPane().add(txtrtext);

		JTextArea txtrtext_1 = new JTextArea();
		txtrtext_1.setBackground(new Color(245, 245, 245));
		txtrtext_1.setEditable(false);
		txtrtext_1.setText(input[1]);
		txtrtext_1.setBounds(221, 87, 431, 78);
		frame.getContentPane().add(txtrtext_1);

		JTextArea txtrtext_2 = new JTextArea();
		txtrtext_2.setBackground(new Color(245, 245, 245));
		txtrtext_2.setEditable(false);
		txtrtext_2.setText(input[2]);
		txtrtext_2.setBounds(221, 178, 431, 136);
		frame.getContentPane().add(txtrtext_2);

		/* labels for medical information */
		JLabel lblImportantNotes = new JLabel("Name: ");
		lblImportantNotes.setBounds(45, 25, 107, 16);
		frame.getContentPane().add(lblImportantNotes);

		JLabel lblCurrentInjuries = new JLabel("Description:");
		lblCurrentInjuries.setBounds(45, 87, 107, 16);
		frame.getContentPane().add(lblCurrentInjuries);

		JLabel lblPastInjuries = new JLabel("Treatment:");
		lblPastInjuries.setBounds(45, 178, 107, 16);
		frame.getContentPane().add(lblPastInjuries);
	}
}
