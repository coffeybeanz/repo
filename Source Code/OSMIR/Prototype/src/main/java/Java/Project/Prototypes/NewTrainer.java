package Java.Project.Prototypes;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;

public class NewTrainer {

	private JFrame frame;
	private JTextField FN;
	private JTextField LN;
	private JTextField E;
	private JPasswordField PWD;
	private JTextField UN;

	public NewTrainer() {
		initialize();
	}

	/* set visibility of window */
	public void setVisibility(boolean flag) {
		frame.setVisible(flag);
	}
	
	/* 
	 * make window and set UI
	 * provides input fields for the personal information of a
	 * new trainer-level user
	 * 
	 *  includes: 
	 *      submit button - saves the text entered in the fields to 
	 *      	the database
	 *      cancel button - exits the page and returns to the previous
	 *      	page without saving
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setBounds(100, 100, 720, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(false);

		/* text fields for the new trainer's personal info */
		FN = new JTextField();
		FN.setBounds(308, 105, 269, 22);
		frame.getContentPane().add(FN);
		FN.setColumns(10);

		UN = new JTextField();
		UN.setColumns(10);
		UN.setBounds(308, 169, 269, 22);
		frame.getContentPane().add(UN);

		LN = new JTextField();
		LN.setBounds(308, 138, 269, 22);
		frame.getContentPane().add(LN);
		LN.setColumns(10);

		E = new JTextField();
		E.setBounds(308, 239, 269, 22);
		frame.getContentPane().add(E);
		E.setColumns(10);

		PWD = new JPasswordField();
		PWD.setBounds(308, 204, 269, 22);
		frame.getContentPane().add(PWD);

		/* options to select sport from */
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(308, 274, 269, 22);
		comboBox.addItem("Football");
		comboBox.addItem("Baseball");
		comboBox.addItem("Basketball");
		comboBox.addItem("Soccer");
		comboBox.addItem("Volleyball");
		frame.getContentPane().add(comboBox);

		/* submit button */
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				File file = new File("Data.csv");
				String out = "";
				Scanner scan;
				try {
					scan = new Scanner(file);
					while (scan.hasNextLine()) {
						out += scan.nextLine();
						out += "\n";
					}
					out += FN.getText() + ",";
					out += LN.getText() + ",";
					out += UN.getText() + ",";
					out += PWD.getText() + ",";
					out += "Trainer,";
					out += E.getText() + ",";
					out += comboBox.getItemAt(comboBox.getSelectedIndex()) + "\n";
					scan.close();
					FileWriter f;
					try {
						f = new FileWriter(file);
						f.write(out);
						f.flush();
						f.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				HeadTrainerTrainers htt = new HeadTrainerTrainers();
				htt.setVisibility(true);
				frame.dispose();
			}
		});
		btnSubmit.setBounds(210, 346, 97, 25);
		frame.getContentPane().add(btnSubmit);

		/* cancel button */
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HeadTrainerTrainers htt = new HeadTrainerTrainers();
				htt.setVisibility(true);
				frame.dispose();
			}
		});
		btnCancel.setBounds(392, 346, 97, 25);
		frame.getContentPane().add(btnCancel);

		/* labels for input fields */
		JLabel lblNewTrainer = new JLabel("New Trainer");
		lblNewTrainer.setBounds(289, 65, 85, 16);
		frame.getContentPane().add(lblNewTrainer);

		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setBounds(160, 108, 85, 16);
		frame.getContentPane().add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setBounds(160, 141, 85, 16);
		frame.getContentPane().add(lblLastName);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(160, 172, 74, 16);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(160, 207, 74, 16);
		frame.getContentPane().add(lblPassword);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(160, 242, 74, 16);
		frame.getContentPane().add(lblEmail);

		JLabel lblSport = new JLabel("Sport:");
		lblSport.setBounds(160, 276, 74, 16);
		frame.getContentPane().add(lblSport);

	}
}
