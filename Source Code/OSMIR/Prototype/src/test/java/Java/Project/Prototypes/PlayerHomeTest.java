package Java.Project.Prototypes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PlayerHomeTest {

	@Test
	void test() {
		Main x = new Main();
	}
	
	@Test
	void InjuryDescription() {
		Injury i = new Injury();
		i.setDescription("Bleh");
		assertEquals(i.getDescription(), "Bleh");
	}
	@Test
	void InjurySetName() {
		Injury i = new Injury();
		i.setName("Bleh");
		assertEquals(i.getName(), "Bleh");
	}
	@Test
	void BadInj() {
		BadInjury i = new BadInjury();
		i.setHospital(true);
		assert(i.getHospital());
	}
	
	@Test
	void BadInj2() {
		BadInjury i = new BadInjury();
		i.setHospital(false);
		assert(!i.getHospital());
	}
	
	@Test
	void TrainerInj() {
		TrainerInjury x = new TrainerInjury();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void TrainerAthletes() {
		TrainerAthletes x = new TrainerAthletes();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void NewTrainer() {
		NewTrainer x = new NewTrainer();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void NewInjury() {
		NewInjury x = new NewInjury("ouchie");
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void NewAthlete() {
		NewAthlete x = new NewAthlete();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void NewAdmin() {
		NewAdmin x = new NewAdmin();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void HeadTrainerTrainers() {
		HeadTrainerTrainers x = new HeadTrainerTrainers();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void HeadTrainerHome() {
		HeadTrainerHome x = new HeadTrainerHome();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void HeadTrainerAthletes() {
		HeadTrainerAthletes x = new HeadTrainerAthletes();
		x.setVisibility(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.setVisibility(false);
	}
	
	@Test
	void GoodInjury() {
		GoodInjury x = new GoodInjury();
		x.setDaysOut(1);
		assertEquals(x.getDaysOut(), 1);
	}
}
